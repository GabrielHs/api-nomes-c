using System.Threading.Tasks;
using geradornames_c.Models;

namespace geradornames_c.Data
{
    public interface IRepository
    {
        //Metodos Genericos

        void Add<T>(T entity) where T : class;

        void Delete<T>(T entity) where T : class;

        Task<bool> SaveChangesAsync();


        Task<PrefixoModel[]> GetAllPrefixos(bool includePrefixo);

        Task<SufixoModel[]> GetAllSufixos(bool includeSufixo);

        Task<PrefixoModel> GetPrefixoId(int id,bool includePrefixo);

        Task<SufixoModel> GetSufixoId(int id,bool includeSufixo);


    }
}