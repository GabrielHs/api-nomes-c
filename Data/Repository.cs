using System.Linq;
using System.Threading.Tasks;
using geradornames_c.Models;
using Microsoft.EntityFrameworkCore;

namespace geradornames_c.Data
{

    public class Repository : IRepository
    {
        public DataContext _context { get; }
        public Repository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() > 0);
        }

        public async Task<PrefixoModel[]> GetAllPrefixos(bool includePrefixo)
        {
            IQueryable<PrefixoModel> query = _context.PrefixoModels;

            if (includePrefixo)
            {
                query = query
                        .Include(pref => pref.Description);
            }

            return await query.ToArrayAsync();
        }

        public async Task<SufixoModel[]> GetAllSufixos(bool includeSufixo)
        {
            {
                IQueryable<SufixoModel> query = _context.SufixoModels;

                if (includeSufixo)
                {
                    query = query
                            .Include(suf => suf.Description);
                }

                return await query.ToArrayAsync();
            }
        }

        public async Task<PrefixoModel> GetPrefixoId(int id, bool includePrefixo)
        {
            IQueryable<PrefixoModel> query = _context.PrefixoModels;

            if (includePrefixo)
            {
                query = query
                        .Include(pref => pref.Description);
            }

            query = query.AsNoTracking()
                        .OrderBy(p => p.Id)
                        .Where(pre => pre.Id == id);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<SufixoModel> GetSufixoId(int id, bool includeSufixo)
        {
            IQueryable<SufixoModel> query = _context.SufixoModels;

            if (includeSufixo)
            {
                query = query
                        .Include(pref => pref.Description);
            }

            query = query.AsNoTracking()
                        .OrderBy(p => p.Id)
                        .Where(pre => pre.Id == id);

            return await query.FirstOrDefaultAsync();
        }
    }
}