using System;
using System.Collections.Generic;
using geradornames_c.Models;
using Microsoft.EntityFrameworkCore;

namespace geradornames_c.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<PrefixoModel> PrefixoModels { get; set; }

        public DbSet<SufixoModel> SufixoModels { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            var listPrefixos = new List<PrefixoModel>();

            var PrefixoModel1 = new PrefixoModel() { Id = 1, Type = "prefix", Description = "Airi", Create = DateTime.UtcNow.ToLongDateString() };

            var PrefixoModel2 = new PrefixoModel() { Id = 2, Type = "prefix", Description = "Jet", Create = DateTime.UtcNow.ToLongDateString() };

            var PrefixoModel3 = new PrefixoModel() { Id = 3, Type = "prefix", Description = "Flitght", Create = DateTime.UtcNow.ToLongDateString() };
            
            listPrefixos = new List<PrefixoModel>(){ PrefixoModel1, PrefixoModel2, PrefixoModel3 };



            var listSufixos = new List<SufixoModel>();

            var SufixoMode1 = new SufixoModel() { Id = 1, Type = "sufix", Description = "Station", Create = DateTime.UtcNow.ToLongDateString() };

            var SufixoModel2 = new SufixoModel() { Id = 2, Type = "sufix", Description = "Hub", Create = DateTime.UtcNow.ToLongDateString() };

            var SufixoModel3 = new SufixoModel() { Id = 3, Type = "sufix", Description = "Mart", Create = DateTime.UtcNow.ToLongDateString() };

           listSufixos = new List<SufixoModel>(){ SufixoMode1, SufixoModel2, SufixoModel3 };


            //Gera carga de valores
            builder.Entity<PrefixoModel>().HasData(listPrefixos.ToArray());
            builder.Entity<SufixoModel>().HasData(listSufixos.ToArray());



        }

    }
}