﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace geradornames_c.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrefixoModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Create = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrefixoModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SufixoModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Create = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SufixoModels", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "PrefixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 1, "segunda-feira, 24 de fevereiro de 2020", "Airi", "prefix" });

            migrationBuilder.InsertData(
                table: "PrefixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 2, "segunda-feira, 24 de fevereiro de 2020", "Jet", "prefix" });

            migrationBuilder.InsertData(
                table: "PrefixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 3, "segunda-feira, 24 de fevereiro de 2020", "Flitght", "prefix" });

            migrationBuilder.InsertData(
                table: "SufixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 1, "segunda-feira, 24 de fevereiro de 2020", "Station", "sufix" });

            migrationBuilder.InsertData(
                table: "SufixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 2, "segunda-feira, 24 de fevereiro de 2020", "Hub", "sufix" });

            migrationBuilder.InsertData(
                table: "SufixoModels",
                columns: new[] { "Id", "Create", "Description", "Type" },
                values: new object[] { 3, "segunda-feira, 24 de fevereiro de 2020", "Mart", "sufix" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrefixoModels");

            migrationBuilder.DropTable(
                name: "SufixoModels");
        }
    }
}
