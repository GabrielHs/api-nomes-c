# Api geradorname

# Start
```
dotnet run
```

# Info
Projeto api para que seja consumida para projeto frontend do gerador de nomes disponivel [Aqui](https://bitbucket.org/GabrielHs/gerador-de-nomes/src/master/)
- Web api .Net Core
- EntityFrameworkCore
- Sqlite

# Important
Consulta de dominios é realizada via web scrapping no site do whois, caso seja feito atualizações do site, será necessario atualizar o projeto 