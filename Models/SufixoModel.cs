namespace geradornames_c.Models
{
    public class SufixoModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string Create { get; set; }
    }
}