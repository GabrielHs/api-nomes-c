namespace geradornames_c.Models
{
    public class DomainModel
    {
        public string Status { get; set; }

        public string Name { get; set; }

        public string Checkout { get; set; }

        public string Extension {get;set;}

    }
}