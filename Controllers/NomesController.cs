using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using geradornames_c.Data;
using geradornames_c.Models;
using System;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace geradornames_c.Controllers
{

    public class NomesController : ControllerBase
    {
        public IRepository _repo { get; set; }

        public NomesController(IRepository repo)
        {
            _repo = repo;

        }


        [Route("api/nomes")]
        [HttpGet]
        public async Task<IActionResult> GetNomes()
        {
            try
            {
                var listaPrefixos = await _repo.GetAllPrefixos(false);

                var listaSufixos = await _repo.GetAllSufixos(false);

                return Ok(new { prefixos = listaPrefixos, sufixos = listaSufixos });
            }
            catch (System.Exception ex)
            {

                return Ok(new { Erro = "ocorreu um erro " + ex.Message });
            }
        }

        [Route("api/prefixo")]
        [HttpPost]
        public async Task<IActionResult> AddPrefixo([FromBody] PrefixoModel model)
        {


            try
            {
                model.Type = "prefix";
                model.Create = DateTime.UtcNow.ToLongDateString();
                _repo.Add(model);
                if (await _repo.SaveChangesAsync())
                {
                    return Ok(new { Ok = $"O {model.Description} foi cadastrado com sucesso" });

                }

            }
            catch (System.Exception ex)
            {

                return Ok(new { Erro = "ocorreu um erro de processamento na api" + ex.Message });
            }

            return BadRequest();
        }

        [Route("api/sufixo")]
        [HttpPost]
        public async Task<IActionResult> AddSufix([FromBody] SufixoModel model)
        {


            try
            {
                model.Type = "sufix";
                model.Create = DateTime.UtcNow.ToLongDateString();
                _repo.Add(model);
                if (await _repo.SaveChangesAsync())
                {
                    return Ok(new { Ok = $"O {model.Description} foi cadastrado com sucesso" });

                }

            }
            catch (System.Exception ex)
            {

                return Ok(new { Erro = "ocorreu um erro de processamento na api" + ex.Message });
            }

            return BadRequest();
        }

        [Route("api/prefixo/delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeletePrefixo(int id)
        {

            try
            {
                if (id != 0)
                {

                    var prefixo = await _repo.GetPrefixoId(id, false);
                    _repo.Delete(prefixo);

                    if (await _repo.SaveChangesAsync())
                    {
                        return Ok(new { OK = "Prefixo excluido" });
                    }


                }
                else
                {
                    return Ok(new { Erro = "id esta nulo" });
                }
            }
            catch (System.Exception ex)
            {

                return Ok(new { Erro = "Ocorreu um erro de processamento" + ex.Message });
            }


            return BadRequest();

        }



        [Route("api/sufixo/delete/{id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteSufixo(int id)
        {

            try
            {
                if (id != 0)
                {

                    var prefixo = await _repo.GetSufixoId(id, false);
                    _repo.Delete(prefixo);

                    if (await _repo.SaveChangesAsync())
                    {
                        return Ok(new { OK = "Sufixo excluido" });
                    }


                }
                else
                {
                    return Ok(new { Erro = "id esta nulo" });
                }
            }
            catch (System.Exception ex)
            {

                return Ok(new { Erro = "Ocorreu um erro de processamento" + ex.Message });
            }


            return BadRequest();

        }



        [Route("api/dominios")]
        [HttpGet]
        public async Task<IActionResult> GetDominios()
        {
            try
            {
                var prefixos = await _repo.GetAllPrefixos(false);

                var sufixos = await _repo.GetAllSufixos(false);

                var listaDomais = new List<DomainModel>();

                var dom = new DomainModel();
                foreach (var pref in prefixos)
                {

                    foreach (var suf in sufixos)
                    {
                        var name = pref.Description + suf.Description;

                        var url = name.ToLower();

                        WebClient client = new WebClient();

                        string htmlCode = client.DownloadString("https://www.whois.com/whois/" + url + ".com.br");
                        UTF8Encoding utf8 = new UTF8Encoding();

                        string unicodeString = htmlCode;

                        Byte[] encodedBytes = utf8.GetBytes(unicodeString);

                        String decodedString = utf8.GetString(encodedBytes);


                        var matcheDomain = Regex.Match(decodedString, @"domain:\s[^>]+?>([^<]+?)/simx");

                        if (matcheDomain.Success)
                        {
                            dom.Status = "Dominio indisponivel";
                            dom.Name = url;
                            dom.Checkout = "https://checkout.hostgator.com.br/?a=add&sld=" + url + "&tld=.com.br";

                        }
                        else
                        {
                            dom.Status = "Dominio disponivel";
                            dom.Name = url;
                            dom.Checkout = "https://checkout.hostgator.com.br/?a=add&sld=" + url + "&tld=.com.br";
                        }


                    }

                    listaDomais.Add(dom);
                }


                return Ok(new { Ok = listaDomais });
            }
            catch (System.Exception ex)
            {

                return Ok("Erro na api " + ex.Message);
            }



        }




        [Route("api/dominio/{name}")]
        [HttpGet]
        public IActionResult GetDominiosDetains(string name)
        {
            try
            {

                var extension = new string[] { ".com.br", ".com", ".net" };
                var listaDomais = new List<DomainModel>();

                var dom = new DomainModel();

                foreach (var ext in extension)
                {

                    var url = name.ToLower();

                    WebClient client = new WebClient();

                    string htmlCode = client.DownloadString("https://www.whois.com/whois/" + url + ext);
                    UTF8Encoding utf8 = new UTF8Encoding();

                    string unicodeString = htmlCode;

                    Byte[] encodedBytes = utf8.GetBytes(unicodeString);

                    String decodedString = utf8.GetString(encodedBytes);


                    var matcheDomain = Regex.Match(decodedString, @"domain:\s[^>]+?>([^<]+?)/simx");

                    if (matcheDomain.Success)
                    {
                        dom.Status = "Dominio indisponivel";
                        dom.Extension = ext;
                        dom.Checkout = "https://checkout.hostgator.com.br/?a=add&sld=" + url + "&tld=" + ext + "";

                    }
                    else
                    {
                        dom.Status = "Dominio disponivel";
                        dom.Extension = ext;
                        dom.Checkout = "https://checkout.hostgator.com.br/?a=add&sld=" + url + "&tld=" + ext + "";
                    }


                }

                listaDomais.Add(dom);



                return Ok(new { Ok = listaDomais });
            }
            catch (System.Exception ex)
            {

                return Ok("Erro na api " + ex.Message);
            }



        }



    }
}